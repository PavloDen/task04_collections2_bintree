package edu.pavlo;

public class DeleteRunnerTask implements Task<String, String> {

  @Override
  public String doTask(String input) {
    System.out.println("Delete task " + input);
    return "task deleted";
  }
}
