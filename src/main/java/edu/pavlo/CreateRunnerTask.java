package edu.pavlo;

public class CreateRunnerTask implements Task<String, String> {

  @Override
  public String doTask(String input) {
    System.out.println("create  task " + input);
    return "task created";
  }
}
