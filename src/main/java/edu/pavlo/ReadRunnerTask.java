package edu.pavlo;

public class ReadRunnerTask implements Task<String, String> {

  @Override
  public String doTask(String input) {
    System.out.println("read task " + input);
    return "task readied";
  }
}
