package edu.pavlo;

import java.util.Map;

public class ShowRunnerTask implements Task<String, String> {

  private Map<MenuItem, Task> menuTasks;

  public ShowRunnerTask(Map<MenuItem, Task> menuTasks) {
    this.menuTasks = menuTasks;
  }

  @Override
  public String doTask(String input) {
    System.out.println("\n Console Menu :");
    for (MenuItem menuItemKey : menuTasks.keySet()) {
      System.out.println(menuItemKey.getId() + " " + menuItemKey.name());
    }
    return null;
  }
}
