package edu.pavlo;

public class UpdateRunnerTask implements Task<String, String> {

  @Override
  public String doTask(String input) {
    System.out.println("Update task " + input);
    return "task updated";
  }
}
