package edu.pavlo;

public class MenuRunner {
  public static void main(String[] args) {

    ConsoleMenu consoleMenu = new ConsoleMenu();
    consoleMenu.init();
    consoleMenu.runMenu();
  }
}
