package edu.pavlo;

public enum MenuItem {
  CREATE(1),
  READ(2),
  UPDATE(3),
  DELETE(4),
  SHOW(5),
  EXIT(6);

  private int id;

  MenuItem(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public static MenuItem getValueById(int numberId) {
    for (MenuItem m : MenuItem.values()) {
      if (m.getId() == numberId) {
        return m;
      }
    }
    return null;
  }
}
