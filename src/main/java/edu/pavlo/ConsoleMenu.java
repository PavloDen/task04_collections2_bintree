package edu.pavlo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ConsoleMenu {

  private Map<MenuItem, Task> menuTasks;

  public ConsoleMenu() {
    menuTasks = new LinkedHashMap<>();
    menuTasks.put(MenuItem.CREATE, new CreateRunnerTask());
    menuTasks.put(MenuItem.READ, new ReadRunnerTask());
    menuTasks.put(MenuItem.UPDATE, new UpdateRunnerTask());
    menuTasks.put(MenuItem.DELETE, new DeleteRunnerTask());
    menuTasks.put(MenuItem.SHOW, new ShowRunnerTask(menuTasks));
    menuTasks.put(MenuItem.EXIT, x -> x);
  }

  public void runMenu() {
    int userChoice;
    Scanner in = new Scanner(System.in);
    List<Integer> menuItemIds =
        menuTasks.keySet().stream().map(MenuItem::getId).collect(Collectors.toList());

    String taskResult;
    do {
      System.out.println("Please, select menu item");
      userChoice = in.nextInt();
      if (!menuItemIds.contains(userChoice)) {
        System.out.println("Please, enter valid value");
        continue;
      } else {
        MenuItem selectedMenuItem = MenuItem.getValueById(userChoice);
        if (selectedMenuItem != null) {
          Task selectedTask = menuTasks.get(selectedMenuItem);
          if (selectedTask != null) {
            taskResult = selectedTask.doTask(" Current Task");
            if (taskResult != null) {
              System.out.println(taskResult);
            }
          } else {
            System.out.println(" Task is not selected");
          }
        } else {
          System.out.println(" Nothing selected in Menu Item");
        }
      }
    } while (userChoice != 6);
  }

  public void init() {
    menuTasks.get(MenuItem.SHOW).doTask(null);
  }

  public static void main(String[] args) {

    ConsoleMenu consoleMenu = new ConsoleMenu();
    consoleMenu.init();
    consoleMenu.runMenu();
  }
}
