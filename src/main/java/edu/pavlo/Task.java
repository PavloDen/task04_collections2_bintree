package edu.pavlo;

public interface Task<T, R> {
  T doTask(R input);
}
